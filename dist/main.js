/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/authorizeWithKlarna.js":
/*!************************************!*\
  !*** ./src/authorizeWithKlarna.js ***!
  \************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
// Submit the payment for authorization with the selected category
/* harmony default export */ __webpack_exports__["default"] = (function () {
  Klarna.Payments.authorize({
    payment_method_category: "pay_later"
  }, function (res) {
    console.log("AUTHORIZE");
    console.log({
      authRes: res
    });

    if (res.approved) {
      console.log({
        approved: res.approved
      });
      console.log("\n                ========\n                APPROVED\n                ========\n            "); // Payment has been authorized
    } else {
      console.log({
        approved: res.approved
      });

      if (res.error) {// Payment not authorized or an error has occurred
      } else {// handle other states
        }
    }
  });
});

/***/ }),

/***/ "./src/getSource.js":
/*!**************************!*\
  !*** ./src/getSource.js ***!
  \**************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (function () {
  Stripe.source.get("src_1E03PNKVDbGemgq8V6ETPoGa", "src_client_secret_ESzONVO1eTVwUOMGH6qDvwiw");
});

/***/ }),

/***/ "./src/index.js":
/*!**********************!*\
  !*** ./src/index.js ***!
  \**********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _stripeV2__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./stripeV2 */ "./src/stripeV2.js");
/* harmony import */ var _stripeV3__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./stripeV3 */ "./src/stripeV3.js");
/* harmony import */ var _authorizeWithKlarna__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./authorizeWithKlarna */ "./src/authorizeWithKlarna.js");
/* harmony import */ var _getSource__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./getSource */ "./src/getSource.js");
/* harmony import */ var _updateSource__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./updateSource */ "./src/updateSource.js");




 // LOAD API V2 IN HTML!!!
// window.onload = () => {
//   stripeV2();
// };
// window.onload = document.getElementById("getPayments").addEventListener("click", stripeV2());

window.onload = function () {
  var containerPayNow = document.getElementById("getPayments");
  var payNowButton = document.getElementById("payNow");
  var payLaterButton = document.getElementById("payLater");
  var payOverTimeButton = document.getElementById("payOverTime");
  var getSourceButton = document.getElementById("getSource");
  var updateSourceButton = document.getElementById("updateSource");
  containerPayNow.addEventListener("click", function () {
    Object(_stripeV2__WEBPACK_IMPORTED_MODULE_0__["default"])();
  });
  payNowButton.addEventListener("click", function () {
    Object(_authorizeWithKlarna__WEBPACK_IMPORTED_MODULE_2__["default"])();
  });
  payLaterButton.addEventListener("click", function () {
    Object(_authorizeWithKlarna__WEBPACK_IMPORTED_MODULE_2__["default"])();
  });
  payOverTimeButton.addEventListener("click", function () {
    Object(_authorizeWithKlarna__WEBPACK_IMPORTED_MODULE_2__["default"])();
  });
  getSourceButton.addEventListener("click", function () {
    Object(_getSource__WEBPACK_IMPORTED_MODULE_3__["default"])();
  });
  updateSourceButton.addEventListener("click", function () {
    Object(_updateSource__WEBPACK_IMPORTED_MODULE_4__["default"])();
  });
  console.log({
    containerPayNow: containerPayNow
  });
}; // LOAD API V3 IN HTML!!!
// window.onload = () => {
//   stripeV3();
// };

/***/ }),

/***/ "./src/klarnaCallback.js":
/*!*******************************!*\
  !*** ./src/klarnaCallback.js ***!
  \*******************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return klarnaAsyncCallback; });
function klarnaAsyncCallback(client_token, payment_method_categories) {
  // Initialize the SDK
  Klarna.Payments.init({
    client_token: client_token
  }); // Load the widget for each payment method category:
  // - pay_later
  // - pay_over_time
  // - pay_now

  var available_categories = payment_method_categories.split(',');
  var _iteratorNormalCompletion = true;
  var _didIteratorError = false;
  var _iteratorError = undefined;

  try {
    for (var _iterator = available_categories[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
      var category = _step.value;
      Klarna.Payments.load({
        container: "#klarna_" + category + "_container",
        payment_method_category: category
      }, function (res) {
        console.log({
          klarnaLoadRes: res
        });

        if (res.show_form) {
          console.log({
            show_form: res.show_form
          }); // this payment_method_category can be used
        } else {
          console.log({
            show_form: res.show_form
          }); // this payment_method_category is not available
        }
      });
    }
  } catch (err) {
    _didIteratorError = true;
    _iteratorError = err;
  } finally {
    try {
      if (!_iteratorNormalCompletion && _iterator.return != null) {
        _iterator.return();
      }
    } finally {
      if (_didIteratorError) {
        throw _iteratorError;
      }
    }
  }
}
;

/***/ }),

/***/ "./src/sourceData.js":
/*!***************************!*\
  !*** ./src/sourceData.js ***!
  \***************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ({
  type: "klarna",
  amount: 816,
  currency: "gbp",
  klarna: {
    product: "payment",
    purchase_country: "GB",
    first_name: "Tamas",
    last_name: "Balint",
    shipping_first_name: "Tamas",
    shipping_last_name: "Balint",
    locale: "en-gb" // optional

  },
  owner: {
    // email: "test+pend-accept-3@example.com", // in 3 seconds successful charge
    // email: "test+pend-reject-3@example.com", // in 3 seconds failed charge
    // email: "test+require_signup@example.com", // sign up pay over time
    // email: "test+red@example.com", // option not available
    // email: "test+denied@stripe.com", // denied
    email: "tamas.balint@missguided.com",
    address: {
      city: "Manchester",
      country: "UK",
      line1: "Flat 56, 386 Deansgate",
      line2: "",
      postal_code: "M3 4LB",
      state: ""
    },
    phone: "07984305111"
  },
  order: {
    items: [{
      type: "sku",
      parent: "AB479202",
      description: "Grey cotton T-shirt",
      quantity: 2,
      currency: "gbp",
      amount: 796
    }, {
      type: "tax",
      description: "Taxes",
      currency: "gbp",
      amount: 20
    }, {
      type: "shipping",
      description: "Free Shipping",
      currency: "gbp",
      amount: 0
    }],
    shipping: {
      address: {
        city: "Manchester",
        country: "UK",
        line1: "Flat 56, 386 Deansgate",
        line2: "",
        postal_code: "M3 4LB",
        state: ""
      },
      carrier: "UPS",
      tracking_number: "",
      // optional
      phone: "07984305111" // optional

    }
  }
});

/***/ }),

/***/ "./src/stripeV2.js":
/*!*************************!*\
  !*** ./src/stripeV2.js ***!
  \*************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _klarnaCallback__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./klarnaCallback */ "./src/klarnaCallback.js");
/* harmony import */ var _sourceData__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./sourceData */ "./src/sourceData.js");
// V2
// LOAD API V2 IN HTML!!!


/* harmony default export */ __webpack_exports__["default"] = (function () {
  Stripe.setPublishableKey("pk_test_bLQ41nZfaYB41jJ2bJMMXEZX"); // Stripe.setPublishableKey("pk_test_jFTBpeAq76Pd8XjNcpSvndty"); // personal

  Stripe.source.create(_sourceData__WEBPACK_IMPORTED_MODULE_1__["default"], function (status, result) {
    if (status === 200) {
      console.log({
        createSourceRes: result
      });
      var _result$klarna = result.klarna,
          client_token = _result$klarna.client_token,
          payment_method_categories = _result$klarna.payment_method_categories;
      Object(_klarnaCallback__WEBPACK_IMPORTED_MODULE_0__["default"])(client_token, payment_method_categories);
    }
  });
});

/***/ }),

/***/ "./src/stripeV3.js":
/*!*************************!*\
  !*** ./src/stripeV3.js ***!
  \*************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _sourceData__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./sourceData */ "./src/sourceData.js");
// V3
// LOAD API V3 IN HTML!!!

/* harmony default export */ __webpack_exports__["default"] = (function () {
  var stripe = new Stripe("pk_test_bLQ41nZfaYB41jJ2bJMMXEZX");
  stripe.createSource(_sourceData__WEBPACK_IMPORTED_MODULE_0__["default"]).then(function (result) {
    console.log({
      result: result
    }); // handle result.error or result.source
  });
});

/***/ }),

/***/ "./src/updateSource.js":
/*!*****************************!*\
  !*** ./src/updateSource.js ***!
  \*****************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (function () {
  var sourceId = "src_1E03PNKVDbGemgq8V6ETPoGa";
  var url = "https://api.stripe.com/v1/sources/".concat(sourceId);
  var secretKey = "sk_test_LsEKhJo9JXwLYw0B3oYWThoM";
  var password = "";
  var data = {
    owner: {
      email: "update.email@mg.com"
    }
  }; // Stripe.source.get(
  //     "src_1E03PNKVDbGemgq8V6ETPoGa",
  //     "src_client_secret_ESzONVO1eTVwUOMGH6qDvwiw"
  // );

  fetch(url, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      "Authorization": "Basic ".concat(base64.encode("".concat(secretKey, ":").concat(password)))
    },
    body: JSON.stringify(data)
  });
});

/***/ })

/******/ });
//# sourceMappingURL=main.js.map