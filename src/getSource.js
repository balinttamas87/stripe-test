export default function() {
    const sourceId = window.stripePayment.sourceId;
    const secretKey = window.stripePayment.client_secret;

    Stripe.source.get(
        sourceId,
        secretKey
    );
}