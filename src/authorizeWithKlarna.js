// Submit the payment for authorization with the selected category
export default function () {
    Klarna.Payments.authorize({
        payment_method_category: "pay_later",
    }, function (res) {
        console.log("AUTHORIZE");
        console.log({ authRes: res });
        if (res.approved) {
            console.log({ approved: res.approved });
            console.log(`
                ========
                APPROVED
                ========
            `);
            // Payment has been authorized
        } else {
            console.log({ approved: res.approved });

            if (res.error) {
                // Payment not authorized or an error has occurred
            } else {
                // handle other states
            }
        }
    })
}