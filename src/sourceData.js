export default {
    type: "klarna",
    amount: 816,
    currency: "gbp",
    klarna: {
      product: "payment",
      purchase_country: "GB",
      first_name: "Tamas",
      last_name: "Balint",
      shipping_first_name: "Tamas",
      shipping_last_name: "Balint",
      locale: "en-gb" // optional
    },
    owner: {
        // email: "test+pend-accept-3@example.com", // in 3 seconds successful charge
        // email: "test+pend-reject-3@example.com", // in 3 seconds failed charge
        // email: "test+require_signup@example.com", // sign up pay over time
        // email: "test+red@example.com", // option not available
        // email: "test+denied@stripe.com", // denied
        email: "tamas.balint@missguided.com",
        address: {
            city: "Manchester",
            country: "UK",
            line1: "Flat 56, 386 Deansgate",
            line2: "",
            postal_code: "M3 4LB",
            state: ""
        },
        phone: "07984305111"
    },
    order: {
      items: [
        {
          type: "sku",
          parent: "AB479202",
          description: "Grey cotton T-shirt",
          quantity: 2,
          currency: "gbp",
          amount: 796
        },
        {
          type: "tax",
          description: "Taxes",
          currency: "gbp",
          amount: 20
        },
        {
          type: "shipping",
          description: "Free Shipping",
          currency: "gbp",
          amount: 0
        }
      ],
      shipping: {
        address: {
            city: "Manchester",
            country: "UK",
            line1: "Flat 56, 386 Deansgate",
            line2: "",
            postal_code: "M3 4LB",
            state: ""
        },
        carrier: "UPS",
        tracking_number: "", // optional
        phone: "07984305111" // optional
      }
    }
  }