export default function klarnaAsyncCallback(client_token, payment_method_categories) {
  // Initialize the SDK
  Klarna.Payments.init({
    client_token: client_token
  });
  // Load the widget for each payment method category:
  // - pay_later
  // - pay_over_time
  // - pay_now
  var available_categories = payment_method_categories.split(',');
  for (var category of available_categories) {
    Klarna.Payments.load({
      container: "#klarna_" + category + "_container",
      payment_method_category: category
    }, function (res) {
      console.log({ klarnaLoadRes: res });
      if (res.show_form) {
        console.log({ show_form: res.show_form });
        // this payment_method_category can be used
      } else {
        console.log({ show_form: res.show_form });
        // this payment_method_category is not available
      }
    });
  }


};