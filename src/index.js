import stripeV2 from "./stripeV2";
import stripeV3 from "./stripeV3";
import authorizeWithKlarna from "./authorizeWithKlarna";
import getSource from "./getSource";
import updateSource from "./updateSource";

// LOAD API V2 IN HTML!!!
// window.onload = () => {
//   stripeV2();
// };
// window.onload = document.getElementById("getPayments").addEventListener("click", stripeV2());
window.onload = function() {
  var containerPayNow = document.getElementById("getPayments");
  var payNowButton = document.getElementById("payNow");
  var payLaterButton = document.getElementById("payLater");
  var payOverTimeButton = document.getElementById("payOverTime");
  var getSourceButton = document.getElementById("getSource");
  var updateSourceButton = document.getElementById("updateSource");

  containerPayNow.addEventListener("click", () => { stripeV2(); });
  payNowButton.addEventListener("click", () => { authorizeWithKlarna(); });
  payLaterButton.addEventListener("click", () => { authorizeWithKlarna(); });
  payOverTimeButton.addEventListener("click", () => { authorizeWithKlarna(); });
  getSourceButton.addEventListener("click", () => { getSource(); });
  updateSourceButton.addEventListener("click", () => { updateSource(); });
  console.log({ containerPayNow });
};
// LOAD API V3 IN HTML!!!
// window.onload = () => {
//   stripeV3();
// };
