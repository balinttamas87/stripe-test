// V2
// LOAD API V2 IN HTML!!!
import klarnaCallback from "./klarnaCallback";
import sourceData from "./sourceData";

export default () => {
  Stripe.setPublishableKey("pk_test_bLQ41nZfaYB41jJ2bJMMXEZX");
  // Stripe.setPublishableKey("pk_test_jFTBpeAq76Pd8XjNcpSvndty"); // personal
  Stripe.source
    .create(sourceData, function(status, result) {
      if (status === 200) {
        console.log({ createSourceRes: result });
        window.stripePayment = {};
        window.stripePayment.client_secret = result.client_secret;
        window.stripePayment.sourceId = result.id;
        
        const { client_token, payment_method_categories } = result.klarna;
        klarnaCallback(client_token, payment_method_categories);
      }
    });
};
